export class MenuConfig {
  public defaults: any = {
    header: {
      self: {},
      items: [
        {
          title: 'หน้าหลัก',
          root: true,
          icon: 'fas fa-home',
          page: '/home'
        },
      ]
    },
    aside: {
      self: {},
      items: [
        {
          title: 'หน้าหลัก',
          root: true,
          icon: 'fas fa-home',
          page: '/home'
        },
        {
          title: 'สมัครสมาชิก',
          root: true,
          bullet: 'dot',
          icon: 'fas fa-user',
          submenu: [
            {
              title: 'สมัครเมมเบอร์',
              page: '/registerMember'
            },
            {
              title: 'สมัครสตาฟ',
              page: '/registerStaff'
            },
          ]
        },
        {
          title: 'ถอนเงิน',
          root: true,
          icon: 'far fa-money-bill-alt',
          page: '/withdraw'
        },
        {
          title: 'รายงานสมาชิก',
          root: true,
          icon: 'fas fa-user',
          page: '/transaction/trxlist'
        },
        {
          title: 'รายงานการฝาก',
          root: true,
          icon: 'fas fa-list-alt',
          page: '/transaction/trxdeposit'
        },
        {
          title: 'รายงานการถอน',
          root: true,
          icon: 'fas fa-list-alt',
          page: '/transaction/trxwithdraw'
        },
        {
          title: 'เพิ่ม/ลด เครดิต',
          root: true,
          icon: 'fas fa-list-alt',
          page: '/credit'
        },
        {
          title: 'รายงานสรุปยอด',
          root: true,
          icon: 'fas fa-list-alt',
          page: '/trxsummary'
        },
        {
          title: 'รายงานที่ถูกซ่อน',
          root: true,
          icon: 'fas fa-list-alt',
          page: '/trxhidden'
        },
        {
          title: 'พนักงาน',
          root: true,
          icon: 'fas fa-user',
          page: '/staff'
        },
        {
          title: 'ธนาคาร',
          root: true,
          icon: 'fas fa-university',
          page: '/bank'
        },
        {
          title: 'รายงานตามสมาชิก',
          root: true,
          icon: 'fas fa-list-alt',
          page: '/trxlistmember'
        },
        // {
        //   title: 'จัดการโปรโมชั่น',
        //   root: true,
        //   icon: 'fas fa-user',
        //   page: '/managementpromo'
        // },
        // {
        //   title: 'ลิงค์สมัครสมาชิก',
        //   root: true,
        //   icon: 'fas fa-user',
        //   page: '/linkregis'
        // },
        // {
        //   title: 'ยอดได้เสียรายสมาชิก',
        //   root: true,
        //   icon: 'fas fa-list-alt',
        //   page: '/transaction/summarymember'
        // },
      ]
    },
  };

  public get configs(): any {
    return this.defaults;
  }
}
