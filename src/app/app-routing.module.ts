// Angular
import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
// Components
import { BaseComponent } from './views/theme/base/base.component';
// Auth
import { AuthGuard } from './core/auth';
import { AdsComponent } from './views/pages/ads/ads.component';
// Adminwallet
import { RegisterMemberComponent } from './views/pages/registerMember/registerMember.component'
import { RegisterStaffComponent } from './views/pages/registerStaff/registerStaff.component';
import { CreditComponent } from './views/pages/credit/credit.component';
import { TrxsummaryComponent } from './views/pages/trxsummary/trxsummary.component';
import { TrxhiddenComponent } from './views/pages/trxhidden/trxhidden.component';
import { ManagementpromoComponent } from './views/pages/managementpromo/managementpromo.component';
import { LinkregisComponent } from './views/pages/linkregis/linkregis.component';
import { WithdrawComponent } from './views/pages/withdraw/withdraw.component';
import { BankComponent } from './views/pages/bank/bank.component';
import { TrxlistmemberComponent } from './views/pages/trxlistmember/trxlistmember.component'
import { HomeComponent } from './views/pages/home/home.component'
import { StaffComponent } from './views/pages/staff/staff.component'


const routes: Routes = [
  {path: 'auth', loadChildren: () => import('./views/pages/auth/auth.module').then(m => m.AuthModule)},
  {path: 'error', loadChildren: () => import('./views/pages/error/error.module').then(m => m.ErrorModule)},
  {
    path: '',
    component: BaseComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'ads',
        component: AdsComponent
      },
      {
        path: 'registerStaff',
        component: RegisterStaffComponent,
      },
      {
        path: 'registerMember',
        component: RegisterMemberComponent,
      },
      {
        path: 'withdraw',
        component: WithdrawComponent,
      },
      {
        path: 'credit',
        component: CreditComponent,
      },
      {
        path: 'trxsummary',
        component: TrxsummaryComponent,
      },
      {
        path: 'trxhidden',
        component: TrxhiddenComponent,
      },
      {
        path: 'managementpromo',
        component: ManagementpromoComponent,
      },
      {
        path: 'linkregis',
        component: LinkregisComponent,
      },
      {
        path: 'bank',
        component: BankComponent,
      },
      {
        path: 'trxlistmember',
        component: TrxlistmemberComponent,
      },
      {
        path:'home',
        component: HomeComponent,
      },
      {
        path:'staff',
        component: StaffComponent,
      },

       {
         path: 'transaction',
         loadChildren: () => import('./views/pages/transaction/transaction.module').then(m => m.TransactionModule),
       },
      {
        path: 'dashboard',
        loadChildren: () => import('./views/pages/dashboard/dashboard.module').then(m => m.DashboardModule),
      },
      {
        path: 'mail',
        loadChildren: () => import('./views/pages/apps/mail/mail.module').then(m => m.MailModule),
      },
      {
        path: 'ecommerce',
        loadChildren: () => import('./views/pages/apps/e-commerce/e-commerce.module').then(m => m.ECommerceModule),
      },
      {
        path: 'ngbootstrap',
        loadChildren: () => import('./views/pages/ngbootstrap/ngbootstrap.module').then(m => m.NgbootstrapModule),
      },
      {
        path: 'material',
        loadChildren: () => import('./views/pages/material/material.module').then(m => m.MaterialModule),
      },
      {
        path: 'user-management',
        loadChildren: () => import('./views/pages/user-management/user-management.module').then(m => m.UserManagementModule),
      },
      {
        path: 'wizard',
        loadChildren: () => import('./views/pages/wizard/wizard.module').then(m => m.WizardModule),
      },
      {
        path: 'builder',
        loadChildren: () => import('./views/theme/content/builder/builder.module').then(m => m.BuilderModule),
      },
      {path: '', redirectTo: 'home', pathMatch: 'full'},
      {path: '**', redirectTo: 'home', pathMatch: 'full'},
    ],
  },
  {path: '**', redirectTo: 'error/403', pathMatch: 'full'},
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
