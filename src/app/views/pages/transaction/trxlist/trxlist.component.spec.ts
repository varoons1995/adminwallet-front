import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrxlistComponent } from './trxlist.component';

describe('TrxlistComponent', () => {
  let component: TrxlistComponent;
  let fixture: ComponentFixture<TrxlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrxlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrxlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
