import { Component, OnInit ,ChangeDetectorRef, ViewChild} from '@angular/core';
import { MatDialog } from '@angular/material/dialog'
import { DataSource } from '@angular/cdk/table';
import { FormControl } from '@angular/forms';
import axios from 'axios'
import { environment } from '../../../../../environments/environment';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/core/reducers';
import { Logout } from 'src/app/core/auth/_actions/auth.actions';

@Component({
  selector: 'kt-trxwithdraw',
  templateUrl: './trxwithdraw.component.html',
  styleUrls: ['./trxwithdraw.component.scss']
})
export class TrxwithdrawComponent implements OnInit {
  token = localStorage.getItem('Token')
  profile =  JSON.parse(localStorage.getItem('Profile'));
  displayedColumnsFirstTable: string[] =['TRANSACTION_WITHDAW_ID','BANK_ACCOUNT_NUMBER','MEMBER_USERNAME','MoneyAccountD','BALANCE','MoneyAccountTest','CREATEDATE','TRANSACTION_STATUS','UFABET_STATUS','TRANSACTION_GEAR','ACTION_BY','SLIP','Reset','WithdrawHand','Reject'];
  TrxWithdrawDataTableSecond ;
  displayedColumns: string[] = ['BANK_TITLE','BANK_ACCOUNT_NUMBER','COUNT_TW','TOTAL_BALANCE',];
  transactions;

  day = new Date().getDate()+1
  setDate = new Date().setDate(this.day)
  date = new Date(this.setDate)
  creatDate = new FormControl(new Date());
  endDate = new FormControl(new Date(this.date));
  
  Trxwithdraw;
  bankowner;
  username="";
  websiteAll;
  agentType = "";
  api = environment.apibackend;
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(public dialog: MatDialog,
    private changeDetectorRefs: ChangeDetectorRef,
    private store: Store<AppState>
     ) {}

  ngOnInit(): void {
    // throw new Error("Method not implemented.");
    axios({
			method: 'get',
      url: this.api+'/website/all',
      headers: {
				'Authorization': 'Bearer '+this.token
			  },
			})
			.then(response => {
			// do something about response
      this.websiteAll = response.data
     // this.creatDate.setValue(this.daySet)
			})
			.catch(err => {
      console.error(err)
      this.changeDetectorRefs.detectChanges();  
      this.store.dispatch(new Logout());
      })
    console.log(this.TrxWithdrawDataTableSecond)
    this.onSearch();

  }
 

    onSearch(){
      let get_frist_date = this.creatDate.value;
      let frist_date = ("0" + get_frist_date.getDate()).slice(-2);
      let frist_month = ("0" + (get_frist_date.getMonth() + 1)).slice(-2);
      let frist_year = get_frist_date.getFullYear();
      let f_date = frist_year + "-" + frist_month + "-" + frist_date;
    
      let get_last_date = this.endDate.value;
      let last_date = ("0" + get_last_date.getDate()).slice(-2);
      let last_month = ("0" + (get_last_date.getMonth() + 1)).slice(-2);
      let last_year = get_last_date.getFullYear();
      let l_date = last_year + "-" + last_month + "-" + last_date;
      axios({
        method: 'post',
        url: this.api+'/withdraw/findbydate',
        headers: {
          'Authorization': 'Bearer '+this.token
          },
        data :{
          "USERNAME":this.username,
          "AGENTTYPE":this.agentType,
          "CREATEDATE":f_date,
          "ENDDATE":l_date
        }
        })
        .then(response => {
        // do something about response
        this.Trxwithdraw = new MatTableDataSource(response.data.trxList)
        this.bankowner = response.data.bankCount
        // if(response.data.sumtrx.length > 0){
        //   this.TRANSACTION_GEAR_AUTO = response.data.sumtrx[0].TRANSACTION_GEAR_AUTO
        //   this.TRANSACTION_GEAR_MANUAL = response.data.sumtrx[0].TRANSACTION_GEAR_MANUAL
        // }
        //this.transactiontop=response.data.sumtrx
        // console.log(this.transactiontop)
        // this.creatDate.setValue(this.daySet)
        this.Trxwithdraw.paginator = this.paginator
        this.changeDetectorRefs.detectChanges();
        console.log(response.data)
        })
        .catch(err => {
        console.error(err)
        this.changeDetectorRefs.detectChanges();  
        this.store.dispatch(new Logout());
        })   
    }

  aleartNote(i) {
    console.log(i)
    alert(i);
    
  }


  async onReset(v){
    console.log(this.Trxwithdraw.data[v])
    await axios({
      
      method: 'post',
      url: 'http://www.alreadydead.net/withdraw/edit',
      headers: {
        'Authorization': 'Bearer '+this.token
      },
      data:{
        "MEMBER_ID":this.Trxwithdraw.data[v].ID,
        "MEMBER_USERNAME":this.Trxwithdraw.data[v].MEMBER_USERNAME,
        "BANK_ACCOUNT_NUMBER":this.Trxwithdraw.data[v].BANK_ACCOUNT_NUMBER,
        "AMOUNT":this.Trxwithdraw.data[v].BALANCE,
        "BANK_NAME":this.Trxwithdraw.data[v].BANKNAME,
        "ACTIONBY":this.profile.STAFFNAME,
        "TRX_WITHDRAW_ID":this.Trxwithdraw.data[v].TRANSACTION_WITHDAW_ID,
        "ACTION":"Reset",
        "TRANSACTION_STATUS": this.Trxwithdraw.data[v].TRANSACTION_STATUS,
        "TRANSACTION_GEAR": this.Trxwithdraw.data[v].TRANSACTION_GEAR,
        "UFABET_STATUS": this.Trxwithdraw.data[v].UFABET_STATUS,
      }
    })
      .then(response => {
        console.log("response: ", response.data)
        // do something about response
        alert(response.data.message)
        this.onSearch();
      })

      .catch(err => {
        console.error(err)
        this.store.dispatch(new Logout());
      })
  }


  async onReject(v){
    console.log(this.Trxwithdraw.data[v])
    await axios({
      
      method: 'post',
      url: this.api+'/withdraw/edit',
      headers: {
        'Authorization': 'Bearer '+this.token
      },
      data:{
        "MEMBER_ID":this.Trxwithdraw.data[v].ID,
        "MEMBER_USERNAME":this.Trxwithdraw.data[v].MEMBER_USERNAME,
        "BANK_ACCOUNT_NUMBER":this.Trxwithdraw.data[v].BANK_ACCOUNT_NUMBER,
        "AMOUNT":this.Trxwithdraw.data[v].BALANCE,
        "BANK_NAME":this.Trxwithdraw.data[v].BANKNAME,
        "ACTIONBY":this.profile.STAFFNAME,
        "TRX_WITHDRAW_ID":this.Trxwithdraw.data[v].TRANSACTION_WITHDAW_ID,
        "ACTION":"Reject",
        "TRANSACTION_STATUS": this.Trxwithdraw.data[v].TRANSACTION_STATUS,
        "TRANSACTION_GEAR": this.Trxwithdraw.data[v].TRANSACTION_GEAR,
        "UFABET_STATUS": this.Trxwithdraw.data[v].UFABET_STATUS,
      }
    })
      .then(response => {
        console.log("response: ", response.data)
        // do something about response
        alert(response.data.message)
        this.onSearch();
      })

      .catch(err => {
        console.error(err)
        this.store.dispatch(new Logout());
      })
  }

  
  async onHand(v){
    console.log(this.Trxwithdraw.data[v])
    await axios({
      
      method: 'post',
      url: this.api+'/withdraw/edit',
      headers: {
        'Authorization': 'Bearer '+this.token
      },
      data:{
        "MEMBER_ID":this.Trxwithdraw.data[v].ID,
        "MEMBER_USERNAME":this.Trxwithdraw.data[v].MEMBER_USERNAME,
        "BANK_ACCOUNT_NUMBER":this.Trxwithdraw.data[v].BANK_ACCOUNT_NUMBER,
        "AMOUNT":this.Trxwithdraw.data[v].BALANCE,
        "BANK_NAME":this.Trxwithdraw.data[v].BANKNAME,
        "ACTIONBY":this.profile.STAFFNAME,
        "TRX_WITHDRAW_ID":this.Trxwithdraw.data[v].TRANSACTION_WITHDAW_ID,
        "ACTION":"Hand",
        "TRANSACTION_STATUS": this.Trxwithdraw.data[v].TRANSACTION_STATUS,
        "TRANSACTION_GEAR": this.Trxwithdraw.data[v].TRANSACTION_GEAR,
        "UFABET_STATUS": this.Trxwithdraw.data[v].UFABET_STATUS,
      }
    })
      .then(response => {
        console.log("response: ", response.data)
        // do something about response
        alert(response.data.message)
        this.onSearch();
      })

      .catch(err => {
        console.error(err)
        this.store.dispatch(new Logout());
      })
  }

  bankBalance(i){
    let balance = parseFloat(this.Trxwithdraw.data[i].BALANCE)
    let bankbalance = parseFloat(this.Trxwithdraw.data[i].BANK_BALANCE)
    return balance+bankbalance ; 
  }

 



}

@Component({
  selector:'dialog-content-example-trxwithdraw',
  templateUrl:'dialog-content-example-trxwithdraw.html'
})
  export class DialogContentExampleDialogTrx{}